Jobber API

    mvn package
    java -jar target/jobber-api-0.0.1-SNAPSHOT.jar

How to tag a person:

    curl -i -X PUT -H "Content-Type:text/uri-list" -d $'https://api.voim.me/tags/1' https://api.voim.me/people/1/tags

See: http://docs.spring.io/spring-data/rest/docs/2.4.0.RELEASE/reference/html/#_supported_http_methods
