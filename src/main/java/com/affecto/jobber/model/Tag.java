package com.affecto.jobber.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "tags")
public class Tag {

    @Id @GeneratedValue
    private Long id;

    private String name;

    @ManyToMany(mappedBy = "tags")
    private List<Person> people;

    @ManyToMany(mappedBy = "tags")
    private List<Project> projects;

}
