package com.affecto.jobber.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "projects")
public class Project {

    @Id @GeneratedValue
    private Long id;

    private String title;

    private String description;

    private String location;

    private Boolean remoteAllowed;

    private Boolean onClientPremises;

    @ManyToMany
    @JoinTable(name = "tags_and_projects",
            joinColumns = {@JoinColumn(name = "project_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "tag_id", referencedColumnName = "id")})
    private List<Tag> tags;

}
