package com.affecto.jobber.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "people")
public class Person {

    @Id @GeneratedValue
    private Long id;

    private String name;

    private String email;

    private String phone;

    private String blurb;

    @ManyToMany
    @JoinTable(name = "tags_and_people",
            joinColumns = {@JoinColumn(name = "person_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "tag_id", referencedColumnName = "id")})
    private List<Tag> tags;

}
