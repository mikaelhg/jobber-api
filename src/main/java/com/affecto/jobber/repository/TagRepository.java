package com.affecto.jobber.repository;

import com.affecto.jobber.model.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "tags")
public interface TagRepository extends JpaRepository<Tag, Long> {

}
