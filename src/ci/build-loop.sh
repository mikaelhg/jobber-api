#!/bin/bash

while [ : ]
do
    git fetch origin master
    git reset --hard FETCH_HEAD
    git clean -df

    mvn clean package
    sleep 1m
done
